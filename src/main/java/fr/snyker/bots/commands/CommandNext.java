package fr.snyker.bots.commands;

import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import fr.snyker.bots.SoNight;
import fr.snyker.bots.audio.AudioPlayerSendHandler;
import fr.snyker.bots.audio.converter.Mp3MusicConverter;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.VoiceChannel;
import net.dv8tion.jda.core.managers.AudioManager;

import java.io.File;

/**
 * Class créée le 04/01/2019 à 21:18
 * par Jullian Dorian
 */
public class CommandNext implements ICommand {
    /**
     * Retourne le nom de la commande
     *
     * @return name
     */
    @Override
    public String getName() {
        return "next";
    }

    /**
     * Retourne la description de la commande
     *
     * @return description
     */
    @Override
    public String getDescription() {
        return null;
    }

    /**
     * Execution de la commande
     *
     * @param channel - Le channel ou l'a commande est éxécuté
     * @param sender  - Le sender qu'il l'a envoyé
     * @param args    - Les arguments en commande
     * @param jda
     */
    @Override
    public void execute(TextChannel channel, Member sender, String[] args, JDA jda) {

        SoNight.musicManager.skipTrack(channel);

    }
}
