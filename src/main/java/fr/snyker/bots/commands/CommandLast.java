package fr.snyker.bots.commands;

import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.VoiceChannel;
import net.dv8tion.jda.core.managers.AudioManager;

/**
 * Class créée le 04/01/2019 à 22:01
 * par Jullian Dorian
 */
public class CommandLast implements ICommand {
    /**
     * Retourne le nom de la commande
     *
     * @return name
     */
    @Override
    public String getName() {
        return "last";
    }

    /**
     * Retourne la description de la commande
     *
     * @return description
     */
    @Override
    public String getDescription() {
        return null;
    }

    /**
     * Execution de la commande
     *
     * @param channel - Le channel ou l'a commande est éxécuté
     * @param sender  - Le sender qu'il l'a envoyé
     * @param args    - Les arguments en commande
     * @param jda
     */
    @Override
    public void execute(TextChannel channel, Member sender, String[] args, JDA jda) {
        VoiceChannel voiceChannel = sender.getVoiceState().getChannel();
        AudioManager audioManager = channel.getGuild().getAudioManager();


    }
}
