package fr.snyker.bots.commands;

import java.util.HashSet;
import java.util.Set;

/**
 * Class créée le 08/12/2018 à 16:22
 * par Jullian Dorian
 */
public class CommandManager {

    private Set<ICommand> commandSet;

    public CommandManager(){
        this.commandSet = new HashSet<>();
        registerCommand(new CommandPlay());
        registerCommand(new CommandNext());
        registerCommand(new CommandJoin());
        registerCommand(new CommandLast());
        registerCommand(new CommandLeave());
        registerCommand(new CommandList());
        registerCommand(new CommandPause());
        registerCommand(new CommandResume());
        registerCommand(new CommandMixer());
        registerCommand(new CommandLoop());
    }

    /**
     * Return the tag of the command
     * @return tag
     */
    public String getTag(){
        return "sn";
    }

    /**
     * Register a command
     * @param command
     */
    private void registerCommand(ICommand command){
        this.commandSet.add(command);
    }

    /**
     * Retrouve une commande par le nom ou la commande
     * @param command
     * @return iCommand
     */
    public ICommand findCommand(Object command){
        ICommand find = null;
        if(command instanceof ICommand){
            for(ICommand iCommand : commandSet){
                if(command == iCommand){
                    find = iCommand;
                    break;
                }
            }
        }

        if(command instanceof String){
            for(ICommand iCommand : commandSet){
                if(iCommand.getName().equals(command)){
                    find = iCommand;
                    break;
                }
            }
        }

        return find;
    }

}
