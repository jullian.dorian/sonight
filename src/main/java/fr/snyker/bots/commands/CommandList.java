package fr.snyker.bots.commands;

import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import fr.snyker.bots.SoNight;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.entities.TextChannel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Class créée le 22/01/2019 à 11:52
 * par Jullian Dorian
 */
public class CommandList implements ICommand {
    /**
     * Retourne le nom de la commande
     *
     * @return name
     */
    @Override
    public String getName() {
        return "list";
    }

    /**
     * Retourne la description de la commande
     *
     * @return description
     */
    @Override
    public String getDescription() {
        return "";
    }

    /**
     * Execution de la commande
     *
     * @param channel - Le channel ou l'a commande est éxécuté
     * @param sender  - Le sender qu'il l'a envoyé
     * @param args    - Les arguments en commande
     * @param jda
     */
    @Override
    public void execute(TextChannel channel, Member sender, String[] args, JDA jda) {

        StringBuilder names = new StringBuilder();

        Collection<AudioTrack> collection = SoNight.musicManager.getGuildAudioPlayer(channel.getGuild()).scheduler.getList();

        for(AudioTrack audioTrack : collection){
            names.append(audioTrack.getInfo().title);
            names.append("\n");
        }

        channel.sendMessage("Nombre de musique chargé : " + collection.size()).queue();
        channel.sendMessage(names).queue();

    }
}
