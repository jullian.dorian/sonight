package fr.snyker.bots.commands;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import fr.snyker.bots.Console;
import fr.snyker.bots.SoNight;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.VoiceChannel;
import net.dv8tion.jda.core.managers.AudioManager;

/**
 * Class créée le 04/01/2019 à 22:05
 * par Jullian Dorian
 */
public class CommandLeave implements ICommand {
    /**
     * Retourne le nom de la commande
     *
     * @return name
     */
    @Override
    public String getName() {
        return "leave";
    }

    /**
     * Retourne la description de la commande
     *
     * @return description
     */
    @Override
    public String getDescription() {
        return null;
    }

    /**
     * Execution de la commande
     *
     * @param channel - Le channel ou l'a commande est éxécuté
     * @param sender  - Le sender qu'il l'a envoyé
     * @param args    - Les arguments en commande
     * @param jda
     */
    @Override
    public void execute(TextChannel channel, Member sender, String[] args, JDA jda) {

        AudioManager audioManager = channel.getGuild().getAudioManager();

        if(audioManager.isConnected()){
            audioManager.closeAudioConnection();
            Console.log("Deconnexion du channel");
        }
    }
}
