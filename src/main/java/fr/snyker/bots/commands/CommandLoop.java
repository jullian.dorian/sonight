package fr.snyker.bots.commands;

import fr.snyker.bots.Parameters;
import fr.snyker.bots.SoNight;
import fr.snyker.bots.audio.GuildMusicManager;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.TextChannel;

/**
 * Class créée le 03/02/2019 à 13:43
 * par Jullian Dorian
 */
public class CommandLoop implements ICommand {
    /**
     * Retourne le nom de la commande
     *
     * @return name
     */
    @Override
    public String getName() {
        return "loop";
    }

    /**
     * Retourne la description de la commande
     *
     * @return description
     */
    @Override
    public String getDescription() {
        return null;
    }

    /**
     * Execution de la commande
     *
     * @param channel - Le channel ou l'a commande est éxécuté
     * @param sender  - Le sender qu'il l'a envoyé
     * @param args    - Les arguments en commande
     * @param jda
     */
    @Override
    public void execute(TextChannel channel, Member sender, String[] args, JDA jda) {

        GuildMusicManager gm = SoNight.musicManager.getGuildAudioPlayer(channel.getGuild());
        boolean val = !gm.scheduler.isRepeating();
        gm.scheduler.setRepeating(val); //On active ou non la boucle

        channel.sendTyping().queue();

        if(val)
            channel.sendMessage("Vous avez activé la boucle sur la playlist.").queue();
        else
            channel.sendMessage("Vous avez désactivé la boucle sur la playlist.").queue();

    }
}
