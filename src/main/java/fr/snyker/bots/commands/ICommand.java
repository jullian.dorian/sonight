package fr.snyker.bots.commands;

import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;

/**
 * Class créée le 08/12/2018 à 16:16
 * par Jullian Dorian
 */
public interface ICommand {

    /**
     * Retourne le nom de la commande
     * @return name
     */
    String getName();

    /**
     * Retourne la description de la commande
     * @return description
     */
    String getDescription();

    /**
     * Autorise le bot à executer la commande
     * @return false by default
     */
    default boolean allowBot(){
        return false;
    }

    /**
     * Autorise seulement ou non les commandes via la CMD
     * @return false by default
     */
    default boolean allowOnlyCmd() {
        return false;
    }

    /**
     * Execution de la commande
     * @param channel - Le channel ou l'a commande est éxécuté
     * @param sender - Le sender qu'il l'a envoyé
     * @param args - Les arguments en commande
     */
    void execute(TextChannel channel, Member sender, String[] args, JDA jda);

}
