package fr.snyker.bots.commands;

import fr.snyker.bots.Console;
import fr.snyker.bots.SoNight;
import fr.snyker.bots.audio.GuildMusicManager;
import fr.snyker.bots.audio.MusicManager;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.TextChannel;

/**
 * Class créée le 03/02/2019 à 12:21
 * par Jullian Dorian
 */
public class CommandMixer implements ICommand {
    /**
     * Retourne le nom de la commande
     *
     * @return name
     */
    @Override
    public String getName() {
        return "mixer";
    }

    /**
     * Retourne la description de la commande
     *
     * @return description
     */
    @Override
    public String getDescription() {
        return null;
    }

    /**
     * Execution de la commande
     *
     * @param channel - Le channel ou l'a commande est éxécuté
     * @param sender  - Le sender qu'il l'a envoyé
     * @param args    - Les arguments en commande
     * @param jda
     */
    @Override
    public void execute(TextChannel channel, Member sender, String[] args, JDA jda) {

        if(args.length > 0) {

            GuildMusicManager guildMusicManager = SoNight.musicManager.getGuildAudioPlayer(channel.getGuild());

            final String arg = args[0];
            int volume = MusicManager.DEFAULT_VOLUME;

            channel.sendTyping().queue();

            if(arg.equalsIgnoreCase("mute") && guildMusicManager.player.getVolume() > 0) {
                volume = 0;
                channel.sendMessage("Suite à un problème de cordes vocales, je suis devenu muet.").queue();
            } else {
                try {
                    volume = Integer.parseInt(arg);
                } catch (NumberFormatException e){
                    Console.log(e.getMessage());
                }

                volume = Math.min(100, Math.max(0, volume));
                channel.sendMessage("Vous avez changé le volume, il est maintenant à : " + volume).queue();
            }

            guildMusicManager.player.setVolume(Math.round(volume));

        }

    }
}
