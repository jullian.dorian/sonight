package fr.snyker.bots.commands;

import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import fr.snyker.bots.Console;
import fr.snyker.bots.SoNight;
import fr.snyker.bots.audio.AudioPlayerSendHandler;
import fr.snyker.bots.audio.converter.Mp3MusicConverter;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.VoiceChannel;
import net.dv8tion.jda.core.managers.AudioManager;

import java.io.File;

/**
 * Class créée le 04/01/2019 à 19:43
 * par Jullian Dorian
 */
public class CommandPlay implements ICommand {
    /**
     * Retourne le nom de la commande
     *
     * @return name
     */
    @Override
    public String getName() {
        return "play";
    }

    /**
     * Retourne la description de la commande
     *
     * @return description
     */
    @Override
    public String getDescription() {
        return "";
    }

    /**
     * Execution de la commande
     *
     * @param channel - Le channel ou l'a commande est éxécuté
     * @param sender  - Le sender qu'il l'a envoyé
     * @param args    - Les arguments en commande
     * @param jda
     */
    @Override
    public void execute(TextChannel channel, Member sender, String[] args, JDA jda) {

        VoiceChannel voiceChannel = sender.getVoiceState().getChannel();

        SoNight.musicManager.getGuildAudioPlayer(channel.getGuild());
        SoNight.musicManager.connectToFirstVoiceChannel(channel.getGuild().getAudioManager(), voiceChannel);

        SoNight.musicManager.loadAndPlay(channel);

    }
}
