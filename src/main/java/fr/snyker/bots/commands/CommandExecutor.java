package fr.snyker.bots.commands;

import fr.snyker.bots.Console;
import fr.snyker.bots.SoNight;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.Event;

/**
 * Class créée le 08/12/2018 à 16:21
 * par Jullian Dorian
 */
public class CommandExecutor {

    private CommandManager commandManager;

    private ICommand current;

    private TextChannel textChannel;
    private Member member;
    private String message;
    private JDA jda;

    private boolean hasTag;
    private String name;
    private String[] args;

    public CommandExecutor(TextChannel textChannel, Member member, String message, JDA jda){
        this.commandManager = SoNight.getCommandManager();
        this.textChannel = textChannel;
        this.member = member;
        this.message = message;
        this.jda = jda;

        String tag = SoNight.getCommandManager().getTag();

        String[] strings = message.split(" ");
        this.hasTag = strings[0].equalsIgnoreCase(tag);
        this.name = strings[1];

        if(strings.length > 1){
            this.args = new String[strings.length-2]; //-2 on enleve le tag et la cmd
            System.arraycopy(strings, 2, this.args, 0, strings.length - 2);
        }

    }

    public CommandExecutor(ICommand command){
        this.current = command;
    }

    /**
     * Checks if the command has the tag
     * @return true if has the tag
     */
    public boolean hasTag(){
        return hasTag;
    }

    public void invoke(){
        if(current == null){
            this.current = commandManager.findCommand(this.name);
        }

        if(current != null)
            this.current.execute(textChannel, member, args, jda);
    }

    public String getName() {
        return name;
    }

    public String getMessage() {
        return message;
    }

    public TextChannel getTextChannel() {
        return textChannel;
    }
}
