package fr.snyker.bots.listeners;

import fr.snyker.bots.Console;
import fr.snyker.bots.SoNight;
import fr.snyker.bots.commands.CommandExecutor;
import net.dv8tion.jda.core.entities.VoiceChannel;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

/**
 * Class créée le 29/12/2018 à 23:42
 * par Jullian Dorian
 */
public class MessageReceivedListener extends ListenerAdapter {

    @Override
    public void onMessageReceived(MessageReceivedEvent event){

        String messageContent = event.getMessage().getContentDisplay();

        if(messageContent.startsWith(SoNight.getCommandManager().getTag())){
            final CommandExecutor commandExecutor = new CommandExecutor(event.getTextChannel(), event.getMember(), messageContent, SoNight.getJda());
            if(commandExecutor.hasTag() && !commandExecutor.getName().isEmpty()){
                commandExecutor.invoke();
            }
        }

    }

}
