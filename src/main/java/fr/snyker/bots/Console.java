package fr.snyker.bots;

import java.awt.*;
import java.time.Instant;
import java.util.Date;

/**
 * Class créée le 29/12/2018 à 23:12
 * par Jullian Dorian
 */
public class Console {

    private static final String HEAD = "[SoNight]: ";

    public static void log(String log){
        System.out.println(HEAD + log);
    }

}
