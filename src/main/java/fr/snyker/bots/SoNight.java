package fr.snyker.bots;

import fr.snyker.bots.audio.MusicManager;
import fr.snyker.bots.audio.converter.ConverterWrapper;
import fr.snyker.bots.audio.converter.IConverter;
import fr.snyker.bots.commands.CommandManager;
import fr.snyker.bots.listeners.MessageReceivedListener;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.Game;

import javax.security.auth.login.LoginException;
import java.io.File;
import java.util.Objects;

/**
 * Class créée le 29/12/2018 à 22:51
 * par Jullian Dorian
 */
public class SoNight {

    private static JDA jda;
    private boolean running = false;

    public static MusicManager musicManager;
    private static CommandManager commandManager;

    public SoNight(){
        try {
            start();
        } catch (LoginException | InterruptedException e) {
            e.printStackTrace();
        }
        this.running = true;

        musicManager = new MusicManager();

        Console.log("Le bot a bien démarré !");
    }

    /**
     * Connexion du bot
     * @throws LoginException
     * @throws InterruptedException
     */
    private void start() throws LoginException, InterruptedException {
        jda = new JDABuilder(AccountType.BOT)
                .setToken("nop")
                .setAudioEnabled(true)
                .setGame(Game.of(Game.GameType.DEFAULT, "danser sur light"))
                .addEventListener(new MessageReceivedListener())
                .buildBlocking();

        commandManager = new CommandManager();
    }

    /**
     * Stop the bot
     */
    public void stop(){
        jda.shutdown();
        running = false;
    }

    public boolean isRunning(){
        return this.running;
    }

    private void loadMusic(){
        final File dir = new File("songs");
        dir.mkdirs();

        for(File file : Objects.requireNonNull(dir.listFiles())){
            addSong(file);
        }
    }

    /**
     * Ajoute une musique dans la file d'attente
     * @param file
     */
    private void addSong(File file) {

        if(file.isFile()) {
            Extension extension = getExtension(file.getName());
            IConverter converter = new ConverterWrapper(file, extension).build();

            Console.log("Musique ajouté à la queue : " + file.getName());
        }

        if(file.isDirectory()) {
            for(File file1 : Objects.requireNonNull(file.listFiles())) {
                addSong(file1);
            }
        }

    }

    public static Extension getExtension(String fileName) {

        final String extension = fileName.substring(fileName.length() - 3);

        switch (extension) {
            case "mp3":
                return Extension.MP3;
            case "mpeg":
                return Extension.MPEG;
            case "flv":
                return Extension.FLV;
            case "wav":
                return Extension.WAV;
        }
        return null;
    }

    public static JDA getJda() {
        return jda;
    }

    public static CommandManager getCommandManager() {
        return commandManager;
    }

    public enum Extension {
        MP3,
        MPEG,
        FLV,
        WAV
    }

}
