package fr.snyker.bots;

import java.util.Scanner;

/**
 * Class créée le 29/12/2018 à 22:48
 * par Jullian Dorian
 *
 * Tag : SN
 */
public class Main {

    private static Scanner scanner;

    public static void main(String[] args) {
        scanner = new Scanner(System.in);

        Console.log("Démarrage du bot en cours...");
        SoNight soNight = new SoNight();

        while(soNight.isRunning()){
            if(scanner.hasNextLine()) {
                String command = scanner.nextLine();
                if(command.equalsIgnoreCase("stop"))
                    soNight.stop();
            }
        }
        scanner.close();
        Console.log("Arrêt du bot.");
    }
}
