package fr.snyker.bots.audio;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.event.AudioEventAdapter;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason;
import fr.snyker.bots.Console;
import fr.snyker.bots.Parameters;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * This class schedules tracks for the audio player. It contains the queue of tracks.
 */
public class TrackScheduler extends AudioEventAdapter {

    private boolean repeating = false;
    private final AudioPlayer player;
    private final BlockingQueue<AudioTrack> queue;
    private AudioTrack lastTrack;

    /**
     * @param player The audio player this scheduler uses
     */
    public TrackScheduler(AudioPlayer player) {
        this.player = player;
        this.queue = new LinkedBlockingQueue<>();
    }

    /**
     * Ajoute une musique dans la queue, si le bot n'a aucune musique en cours, la lance
     * @param track
     */
    public void queue(AudioTrack track) {
        if (!player.startTrack(track, true)) {
            queue.offer(track);
        }
    }

    public void add(AudioTrack track){
        queue.add(track);
    }

    /**
     * Start the next track, stopping the current one if it is playing.
     */
    public void nextTrack() {
        // Start the next track, regardless of if something is already playing or not. In case queue was empty, we are
        // giving null to startTrack, which is a valid argument and will simply stop the player.
        AudioTrack audioTrack = queue.poll();
        player.startTrack(audioTrack, false);

        if(audioTrack != null) {
            Console.log("Playing now: " + audioTrack.getInfo().title);
        }

        //lasts.add(audioTrack);
    }

    public void prevTrack() {

    }

    /**
     * Return the first track on the queue
     * @return AudioTrack
     */
    public AudioTrack firstTrack() {
        try {
            return queue.take();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void reload() {
        player.stopTrack();
        Console.log("reload");
    }

    @Override
    public void onTrackEnd(AudioPlayer player, AudioTrack track, AudioTrackEndReason endReason) {

        this.lastTrack = track;

        // Only start the next track if the end reason is suitable for it (FINISHED or LOAD_FAILED)
        if (endReason.mayStartNext) {
            if(isRepeating() && queue.size() == 0) {
                player.startTrack(lastTrack.makeClone(), false);
            } else {
                nextTrack();
                Console.log("start next");
            }

        } else {
            Console.log("fin");
        }

        Console.log("Reste : " + queue.size());
    }

    public void setRepeating(boolean repeating) {
        this.repeating = repeating;
    }

    public boolean isRepeating() {
        return repeating;
    }

    /**
     * Returne the current queue
     * @return queue
     */
    public Collection<AudioTrack> getList() {
        return queue;
    }

}