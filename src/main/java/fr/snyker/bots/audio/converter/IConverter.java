package fr.snyker.bots.audio.converter;

import com.sedmelluq.discord.lavaplayer.track.BaseAudioTrack;

/**
 * Class créée le 05/01/2019 à 16:52
 * par Jullian Dorian
 */
public interface IConverter<T extends BaseAudioTrack> {

    public T build();
}
