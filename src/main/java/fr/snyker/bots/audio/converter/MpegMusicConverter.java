package fr.snyker.bots.audio.converter;

import com.sedmelluq.discord.lavaplayer.container.mpeg.MpegAudioTrack;

import java.io.File;

/**
 * Class créée le 04/01/2019 à 21:11
 * par Jullian Dorian
 */
public class MpegMusicConverter extends Converter<MpegAudioTrack>{

    public MpegMusicConverter(File file) {
        super(file);
    }

    @Override
    public MpegAudioTrack build() {
        return new MpegAudioTrack(getAudioTrackInfo(), getSeekableInputStream());
    }
}
