package fr.snyker.bots.audio.converter;

import com.sedmelluq.discord.lavaplayer.container.mp3.Mp3AudioTrack;
import com.sedmelluq.discord.lavaplayer.tools.io.SeekableInputStream;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo;

/**
 * Class créée le 08/03/2019 à 12:09
 * par Jullian Dorian
 */
public class SpMp3AudioTrack extends Mp3AudioTrack {

    protected final SeekableInputStream inputStream;

    public SpMp3AudioTrack(AudioTrackInfo trackInfo, SeekableInputStream inputStream) {
        super(trackInfo, inputStream);
        this.inputStream = inputStream;
    }

    @Override
    public AudioTrack makeClone() {
        return new SpMp3AudioTrack(trackInfo, inputStream);
    }
}
