package fr.snyker.bots.audio.converter;

import fr.snyker.bots.SoNight;

import java.io.File;

/**
 * Class créée le 05/01/2019 à 17:01
 * par Jullian Dorian
 */
public class ConverterWrapper {

    private File file;
    private SoNight.Extension extension;

    public ConverterWrapper(File file, SoNight.Extension extension) {
        this.file = file;
        this.extension = extension;
    }

    public Converter build() {
        switch (extension) {
            case MP3:
                return new SpMp3MusicConverter(this.file);
            case MPEG:
                return new MpegMusicConverter(this.file);
            case WAV:
                return new WavMusicConverter(this.file);
        }
        return null;
    }

}
