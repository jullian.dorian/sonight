package fr.snyker.bots.audio.converter;

import java.io.File;

/**
 * Class créée le 04/01/2019 à 21:11
 * par Jullian Dorian
 */
public class SpMp3MusicConverter extends Converter<SpMp3AudioTrack>{

    public SpMp3MusicConverter(File file) {
        super(file);
    }

    @Override
    public SpMp3AudioTrack build() {
        return new SpMp3AudioTrack(getAudioTrackInfo(), getSeekableInputStream());
    }

}
