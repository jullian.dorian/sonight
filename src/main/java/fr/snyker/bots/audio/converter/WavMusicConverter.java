package fr.snyker.bots.audio.converter;

import com.sedmelluq.discord.lavaplayer.container.wav.WavAudioTrack;

import java.io.File;

/**
 * Class créée le 04/01/2019 à 21:11
 * par Jullian Dorian
 */
public class WavMusicConverter extends Converter<WavAudioTrack>{

    public WavMusicConverter(File file) {
        super(file);
    }

    @Override
    public WavAudioTrack build() {
        return new WavAudioTrack(getAudioTrackInfo(), getSeekableInputStream());
    }
}
