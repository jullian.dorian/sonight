package fr.snyker.bots.audio.converter;

import com.sedmelluq.discord.lavaplayer.container.mp3.Mp3AudioTrack;
import java.io.File;

/**
 * Class créée le 04/01/2019 à 21:11
 * par Jullian Dorian
 */
public class Mp3MusicConverter extends Converter<Mp3AudioTrack>{

    public Mp3MusicConverter(File file) {
        super(file);
    }

    @Override
    public Mp3AudioTrack build() {
        return new Mp3AudioTrack(getAudioTrackInfo(), getSeekableInputStream());
    }

}
