package fr.snyker.bots.audio.converter;

import com.sedmelluq.discord.lavaplayer.source.local.LocalSeekableInputStream;
import com.sedmelluq.discord.lavaplayer.tools.io.SeekableInputStream;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo;
import com.sedmelluq.discord.lavaplayer.track.BaseAudioTrack;

import java.io.File;

/**
 * Class créée le 05/01/2019 à 16:56
 * par Jullian Dorian
 */
public abstract class Converter<T extends BaseAudioTrack> implements IConverter<T> {

    private final File file;
    private final AudioTrackInfo audioTrackInfo;
    private final SeekableInputStream seekableInputStream;

    protected Converter(File file) {
        this.file = file;

        final String name = file.getName().substring(0, file.getName().length() - 4);

        this.audioTrackInfo = new AudioTrackInfo(
                name, "", file.length(), "local: " + file.getAbsolutePath(), false, file.toURI().toString()
        );

        this.seekableInputStream = new LocalSeekableInputStream(file);
    }

    public File getFile() {
        return file;
    }

    public AudioTrackInfo getAudioTrackInfo() {
        return audioTrackInfo;
    }

    public SeekableInputStream getSeekableInputStream() {
        return seekableInputStream;
    }

}
