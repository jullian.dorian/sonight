package fr.snyker.bots.audio;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers;
import com.sedmelluq.discord.lavaplayer.source.http.HttpAudioSourceManager;
import com.sedmelluq.discord.lavaplayer.source.local.LocalAudioSourceManager;
import com.sedmelluq.discord.lavaplayer.source.youtube.YoutubeAudioSourceManager;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import fr.snyker.bots.Console;
import fr.snyker.bots.SoNight;
import fr.snyker.bots.audio.converter.ConverterWrapper;
import fr.snyker.bots.audio.converter.IConverter;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.VoiceChannel;
import net.dv8tion.jda.core.managers.AudioManager;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Class créée le 22/01/2019 à 13:37
 * par Jullian Dorian
 */
public class MusicManager {

    public static final int DEFAULT_VOLUME = 50;

    private final AudioPlayerManager playerManager;
    private final Map<Long, GuildMusicManager> musicManagers;

    public MusicManager(){
        this.musicManagers = new HashMap<>();

        this.playerManager = new DefaultAudioPlayerManager();
        AudioSourceManagers.registerRemoteSources(playerManager);
        AudioSourceManagers.registerLocalSource(playerManager);
        playerManager.registerSourceManager(new YoutubeAudioSourceManager());
        playerManager.registerSourceManager(new HttpAudioSourceManager());
        playerManager.registerSourceManager(new LocalAudioSourceManager());
    }

    /**
     * Get the Music Manager from the guild
     * @param guild
     * @return GuildMusicManager
     */
    public synchronized GuildMusicManager getGuildAudioPlayer(Guild guild) {
        long guildId = Long.parseLong(guild.getId());
        GuildMusicManager musicManager = musicManagers.get(guildId);

        if (musicManager == null) {
            musicManager = new GuildMusicManager(playerManager);
            musicManagers.put(guildId, musicManager);
        }

        guild.getAudioManager().setSendingHandler(musicManager.getSendHandler());

        return musicManager;
    }

    private void loadMusic(GuildMusicManager musicManager){
        final File dir = new File("songs");
        dir.mkdirs();

        for(File file : Objects.requireNonNull(dir.listFiles())){
            addSong(file, musicManager);
        }
    }

    /**
     * Add music on the queue
     * @param file
     */
    private void addSong(File file, GuildMusicManager musicManager) {

        if(file.isFile()) {
            SoNight.Extension extension = SoNight.getExtension(file.getName());
            IConverter converter = new ConverterWrapper(file, extension).build();

            Console.log("Musique ajouté à la queue : " + file.getName());
            musicManager.scheduler.add(converter.build());
        }

        if(file.isDirectory()) {
            for(File file1 : Objects.requireNonNull(file.listFiles())) {
                addSong(file1, musicManager);
            }
        }

    }

    /**
     * Loads the music files and start the queue
     * @param channel
     */
    public void loadAndPlay(final TextChannel channel){

        GuildMusicManager musicManager = getGuildAudioPlayer(channel.getGuild());

        Console.log("Chargement des musiques");
        loadMusic(musicManager);
        Console.log("Fin du chargement");

        AudioTrack audioTrack = musicManager.scheduler.firstTrack();

        if(audioTrack != null){
            musicManager.player.startTrack(audioTrack, true);

            /*playerManager.loadItem(audioTrack.getIdentifier(), new AudioLoadResultHandler() {

                @Override
                public void trackLoaded(AudioTrack track) {
                    channel.sendMessage("Adding to queue " + track.getInfo().title).queue();

                    play(channel.getGuild(), musicManager, track);
                }

                @Override
                public void playlistLoaded(AudioPlaylist playlist) {
                    AudioTrack firstTrack = playlist.getSelectedTrack();

                    if (firstTrack == null) {
                        firstTrack = playlist.getTracks().get(0);
                    }

                    channel.sendMessage("Adding to queue " + firstTrack.getInfo().title + " (first track of playlist " + playlist.getName() + ")").queue();

                    play(channel.getGuild(), musicManager, firstTrack);
                }

                @Override
                public void noMatches() {
                }

                @Override
                public void loadFailed(FriendlyException exception) {
                    channel.sendMessage("Impossible de jouer cette musique. Raison : " + exception.getMessage()).queue();
                }
            });*/
        } else {
            channel.sendMessage("Aucune musique dans la playlist").queue();
        }

    }

    public void resume(final TextChannel channel) {
        GuildMusicManager musicManager = getGuildAudioPlayer(channel.getGuild());

        musicManager.player.setPaused(false);
    }

    public void pause(final TextChannel channel) {
        GuildMusicManager musicManager = getGuildAudioPlayer(channel.getGuild());

        musicManager.player.setPaused(true);
    }

    /*
    private void loadAndPlay(final TextChannel channel, final String trackUrl) {
        GuildMusicManager musicManager = getGuildAudioPlayer(channel.getGuild());

        playerManager.loadItemOrdered(musicManager, trackUrl, new AudioLoadResultHandler() {
            @Override
            public void trackLoaded(AudioTrack track) {
                channel.sendMessage("Adding to queue " + track.getInfo().title).queue();

                play(channel.getGuild(), musicManager, track);
            }

            @Override
            public void playlistLoaded(AudioPlaylist playlist) {
                AudioTrack firstTrack = playlist.getSelectedTrack();

                if (firstTrack == null) {
                    firstTrack = playlist.getTracks().get(0);
                }

                channel.sendMessage("Adding to queue " + firstTrack.getInfo().title + " (first track of playlist " + playlist.getName() + ")").queue();

                play(channel.getGuild(), musicManager, firstTrack);
            }

            @Override
            public void noMatches() {
                channel.sendMessage("Nothing found by " + trackUrl).queue();
            }

            @Override
            public void loadFailed(FriendlyException exception) {
                channel.sendMessage("Could not play: " + exception.getMessage()).queue();
            }
        });
    }
    */

    /**
     * Play the track music
     * @param guild
     * @param musicManager
     * @param track
     */
    public void play(Guild guild, GuildMusicManager musicManager, AudioTrack track) {
        connectToFirstVoiceChannel(guild.getAudioManager());

        musicManager.scheduler.queue(track);
    }

    /**
     * Called to next track from the queue
     * @param channel
     */
    public void skipTrack(final TextChannel channel) {
        GuildMusicManager musicManager = getGuildAudioPlayer(channel.getGuild());

        Console.log("Called skip");

        musicManager.scheduler.nextTrack();

        channel.sendMessage("Skipped to next track.").queue();
    }

    /**
     * Called to previous track from the queue
     * @param channel
     */
    public void prevTrack(final TextChannel channel){
        GuildMusicManager musicManager = getGuildAudioPlayer(channel.getGuild());

        Console.log("Called prev");

        musicManager.scheduler.prevTrack();

        channel.sendMessage("Skipped to previous track.").queue();
    }

    /**
     * @see #connectToFirstVoiceChannel(AudioManager, VoiceChannel)
     * @param audioManager
     */
    public void connectToFirstVoiceChannel(AudioManager audioManager) {
        if (!audioManager.isConnected() && !audioManager.isAttemptingToConnect()) {
            for (VoiceChannel voiceChannel : audioManager.getGuild().getVoiceChannels()) {
                audioManager.openAudioConnection(voiceChannel);
                break;
            }
        }
    }

    /**
     * Connect the bot on the channel
     * @param audioManager
     * @param voiceChannel
     * @return
     */
    public boolean connectToFirstVoiceChannel(AudioManager audioManager, VoiceChannel voiceChannel) {
        if(voiceChannel != null) {
            if (!audioManager.isConnected() && !audioManager.isAttemptingToConnect()) {
                audioManager.openAudioConnection(voiceChannel);
                return true;
            }
        }
        return false;
    }

}
